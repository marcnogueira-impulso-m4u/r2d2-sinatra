
require 'bundler/setup'
Bundler.require(:default)

require File.dirname(__FILE__) + "/app/controllers/application_controller.rb"

# use Rack::MethodOverride

run ApplicationController
require_relative 'redis_client'
require_relative 'dynamo_client'

class ServiceIdResponse
  def initialize(service_id, redis_url)
    @service_id = service_id
    @redis_url = redis_url
    # Busca o resultado no Redis, se voltar vazio busca no DynamoDB.
    @data = get_redis_result || get_dynamo_result
  end

  # Busca o resultado no Redis
  def get_redis_result
    @redis = RedisClient.new(@redis_url)
    @redis.get(@service_id)
  end

  # Busca o resultado no DynamoDB, se não voltar vazio grava no Redis.
  def get_dynamo_result
    @dynamo = DynamoClient.find(@service_id)
    @redis.put(@service_id, @dynamo.data) if @dynamo
    @dynamo ? @dynamo.data : nil
  end

  def service_id_data
    @data
  end
end
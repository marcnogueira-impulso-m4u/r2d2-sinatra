
# Classe do DynamoID, funcionamento das queries semelhante ao activerecord.
class DynamoClient
  include Dynamoid::Document

  table name: :service_ids, key: :service_id, read_capacity: 5, write_capacity: 5

  field :service_id, :set, of: :integer
  field :data, :set, of: { serialized: { serializer: JSON } }
end

# DynamoClient.find(id)
# DynamoClient.where(data: foo, )
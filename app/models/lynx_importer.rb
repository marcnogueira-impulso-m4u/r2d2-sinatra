require 'httparty'
require 'json'
require_relative 'dynamo_client'

class LynxImporter
  def self.importer
    @options = { query: { details: 'something' } }
    @service_ids = [] #aqui vão os services ids
    return nil if @service_ids == []
    @service_ids.each do |service_id|
      response = HTTParty.get("#{url}/#{service_id}", @options)
      DynamoClient.create(service_id: service_id, data: response.to_json)
    end
  end
end
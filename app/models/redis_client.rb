require 'redis'
require 'json'

class RedisClient
  def initialize(redis_url)
    @redis = Redis.new(:host => redis_url, :port => 6379)
  end

  def get service_id
    @redis.get service_id
  end

  def put service_id, data
    @redis.set service_id, data
  end

  def delete service_id
    ####
  end
end
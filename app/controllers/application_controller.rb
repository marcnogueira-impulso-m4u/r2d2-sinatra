require "sinatra/base"
require "sinatra/config_file"
require 'aws-sdk'
require 'dynamoid' 
# TODO: ver detalhes da configuração
# -> https://github.com/Dynamoid/Dynamoid
# -> https://github.com/mdzhang/sinatra-dynamoid-dynamodb-hello-world/blob/master/app.rb
require_relative '../models/service_id_response'
require_relative '../models/lynx_importer'

class ApplicationController < Sinatra::Base
  register Sinatra::ConfigFile

  # arquivo com as constantes de cada ambiente
  config_file '../../config/config.yml'

  configure do
    set :public_folder, 'public'
  end

  # DynamoDBConfig -------------------------------------------------#

  Aws.config.update({
    region: 'us-west-2',
    credentials: Aws::Credentials.new(settings.dynamo_access, settings.dynamo_secret)
  })

  Dynamoid.configure do |config|
    config.adapter = 'aws_sdk_v2'
    config.namespace = "r2d2_development"
    config.warn_on_scan = true
    config.read_capacity = 5
    config.write_capacity = 5
    config.endpoint = "http://#{settings.dynamo_ip}:8000"
  end

  #-----------------------------------------------------------#

  # Controllers / Rotas --------------------------------------#
  before do
    content_type :json
  end

  get '/' do
    {message: 'R2D2 Active!'}.to_json
  end

  # Rota principal para exibir o service_id requisitados.
  get '/service_ids/:service_id' do
    r = ServiceIdResponse.new(
      params[:service_id],
      settings.redis_url
    ).service_id_data
    status r ? 200 : 404
    r ? r.to_json : {message: "Service ID not found"}.to_json
  end

  # Importar base do lynx-o
  put '/importar/:key' do
    return {message: 'Chave inválida'}.to_json if params[:key] != '1234'
    begin
      data = LynxImporter.importer
      return {message: 'Não existem services ids para importar'}.to_json if data == nil
    rescue
      status 500
      return {message: 'Erro durante a importação'}.to_json
    {message: 'Base importada com sucesso!'}.to_json
  end
end